To install after Arch:

chromium
cava
lxappearance
xcompmgr
pavucontrol
playonlinux
network-manager-applet
noto-fonts-emoji
tlp tlp-rdw
steam
xorg-xkill
vim
ranger
bleachbit

AUR:

i3-gaps
i3blocks-gaps
arc-gtk-theme
arch-silence-grub-theme
neofetch
ttf-font-awesome
pepper-flash
papirus-icon-theme-git
i3lock-fancy-git
light-git
rofi
spotify